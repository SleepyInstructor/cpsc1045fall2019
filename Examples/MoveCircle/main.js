//Print message to console
//for debugging
console.log("main.js executing");

function sliderXlistener(event){
    let sliderX = document.querySelector("#rangeX");
    let circle = document.querySelector("#circle");
    circle.setAttribute("cx", sliderX.value);
    console.log(circle); //for debugging, so we can see the circle in the console
}

function sliderYlistener(event){
    let sliderY = document.querySelector("#rangeY");
    let circle = document.querySelector("#circle");
    circle.setAttribute("cy", sliderY.value);
    console.log(circle); //for debugging, so we can see the circle in the console  
}
function changeColor(event){
   let colorPicker = document.querySelector("#color");
   let circle = document.querySelector("#circle");
   circle.setAttribute("fill", colorPicker.value);
}